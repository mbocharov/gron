
# GRON


Инструмент для автоматического поиска и запуска группы плейбуков. В данный момент используется для раскатки сертификатов, но в теории его можно использовать для чего угодно, что требует запуска группы плейбуков.

## Установка

```
pip3 install -r requirements.txt
```


## ОПРЕДЕЛЕНИЯ

***Deployment Group*** - строка, объединяющая плейбуки. В данном случае в ней содержится название домена и дополнительные данные, указанные через нижнее подчеркивание (напр. domain.ru_letsencrypt). Другими словами, deployment group - это идентификатор связывающий плейбуки, которые раскатывают один и тот же сертификат. Если мы говорим gron запустить плейбуки из указанной DG и раскатать с помощью них сертификаты, то эти плейбуки должны раскатать один и тот же сертификат на все свои сервера.

***Deployment Task*** - строка, указывающая на тип задачи, который запускается в рамках DG. Например, _deploy_cert - раскатать сертификат, _certbot_acme - выполнить acme challenge. Эти команды захардкожены.

## ОПИСАНИЕ

Программа ищет все yaml файлы, отбирает те из них, которые похожи на плейбуки валидные yaml файлы со списком элементов, которые содержат элемент hosts и которые содержат специальные переменные (deployment tasks), после чего запускает их как плейбуки с указанными агрументами и тегами. Нужна для группировки задач в разных плейбуках, напр. тех, которые отвечают за раскатку определенного сертификата.

Получить текущие метаданные из файлов и вывести в yaml формате

```
#Показать всю метадату
gron -s --root-dir /home/mbocharov/ansible-ng
#Показать только deployment groups
gron -sg --root-dir /home/mbocharov/ansible-ng
```



Пример настройки деплоя сертификата через запуск плейбука с тегом certs и аргументом '-e variable=VARIABLE': 

```
- hosts: someserver.domain.ru
  vars:
    _deploy_cert:                               # <-- deployment task
      deployment_groups:
        - dg: domain.ru_year                       # <-- deployment group 
          tags: ['certs']                       # <-- Тэги, с которыми будет запущен плейбук
          args: ['-e','variable="VARIABLE"']    # <-- Дополнительные аргументы, которые будут подставлены в команду    
          variable: "VARIABLE"                  # <-- Так можно указывать переменные (аналог -e variable="VARIABLE").
```

Так же можно загрузить переменные с помощью vars_files, при этом все переменные из каждого файла добавятся в vars.

Пример:

```
- hosts: someserver.domain.ru
  vars:
    _vars_files: yes #Если эта переменная есть с любым значением - будут загружаться файлы из vars_files
  vars_files:
      - '../vars/gron.yml' #Если нет начального / (путь относителен), то вместо первой части пути (здесь ..) будет подставлен путь к плейбуку
```
    

В каждом плейбуке, который отвечает за раскатку годового domain.ru сертификата, требуется указать Deployment Group `domain.ru_year` с задачей `_deploy_cert` и тогда Gron, найдя их, последовательно запустит. Чтобы сертификат был гарантированно одинаковым на всех эти хостах - требуется забирать его из Vaut.

Можно указывать ***глобальные переменные*** - _deployment_groups, tags, args.

Если указать ***_deployment_groups*** в корне vars, то все группы будут участвовать в каждой задаче.

Пример, все группы из ***_deployment_groups*** будут использованы в задачах ***_certbot_acme, _certbot_upload, _deploy_cert***:

```
- hosts: "some-hosts"
  vars:
    _certbot_acme:
      tags: ['acme_challenge']
      nolimit: true
      deployment_groups: "#GLOBAL"
    _certbot_upload:
      tags: ['certbot_upload']
      nolimit: true
      deployment_groups: "#GLOBAL"
    _deploy_cert:
      tags: ['gcore_upload']
      nolimit: true
      deployment_groups: "#GLOBAL"
    _deployment_groups:
      - dg: 'domain1.ru_letsencrypt'
        domains: ['domain1.ru','*.domain1.ru']
      - dg: 'domain2.ru_letsencrypt'
        domains: ['domain2.ru','*.domain2.ru']
```

Если указать tags и args внутри DT, то агрументы и теги будут применены к каждой DG, которая участвует в задаче.

Пример:

```
- hosts: "someserver.domain.ru"
  vars:
    _deploy_cert:
      tags: ['nginx_conf']
      args: ['-C', '-vvvv']
      deployment_groups:
        - dg: domain1.ru_letsencrypt
        - dg: domain2.ru_letsencrypt

#Аналогично
- hosts: "someserver.domain.ru"
  vars:
    _deploy_cert:
      deployment_groups:
        - dg: domain1.ru_letsencrypt
          tags: ['nginx_conf']
          args: ['-C', '-vvvv']
        - dg: domain2.ru_letsencrypt
          tags: ['nginx_conf']
          args: ['-C', '-vvvv']
```

Так же поддерживаются следующие флаги:

    nolimit - не выставлять аргумент -l 'hosts' при запуске плейбука. Нужно для запуска на локалхосте.
    notags - разрешить запуск без указания тегов раскатки. По-умолчанию плейбук без них запустить нельзя.

Пример:

```
- hosts: 127.0.0.1
  vars:
    _deploy_cert:
      nolimit: yes
      notags: yes
      deployment_groups:
        - dg: domain1.ru_letsencrypt
```

Все переменные, указанные внутри Deployment group, кроме args, tags, hosts, playbook, будут переданы в команду запуска плейбука через -e аргументы

Пример:

```
- hosts: 127.0.0.1
  vars:
    _certbot_acme:
      tags: ['acme_dns']
      nolimit: true
    _deployment_groups:
      - dg: 'domain1.ru_letsencrypt'
        zone_file: "/etc/namedb/master/domain1.ru"
```

Здесь будет запущен плейбук с аргументами:

```
ansible-playbook ... -t acme_dns -e 'zone_file=/etc/namedb/master/domain1.ru' 
```


