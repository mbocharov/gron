import yaml
from pathlib import Path
import pprint
import logging
from multiprocessing.pool import ThreadPool
from deployment import DeploymentGroups
import os
import copy



logger = logging.getLogger('discovery')

def get_deployment_groups(config):
    '''
    Главная функция
    Парсит yaml файлы, возвращает структуру deployment groups 
    Требуется разместить в плейбуке, в vars для выборки hosts, следующее:
        vars:
            <deployment_task>:
                deployment_groups:
                    - dg: domain.ru_year #REQUIRED
                      tags: ['tag1','tag2'] #REQUIRED
                      args: ['arg1', 'arg2']
    Возвращает структуру, описанную в комментарии к _find_deployment_groups
    '''
    yaml_files = _find_yaml_files(config)
    raw_deployment_groups = _find_deployment_groups(yaml_files, config)
    deployment_groups = DeploymentGroups(raw_deployment_groups, config)
    return deployment_groups

def _find_deployment_groups(yaml_files, config):
    #Ищет в yaml файлах таски и собирает все данные из них в структуру deployment_groups
    '''
    yaml_files : set | list
        Список с путями к yaml файлам
    config : dict

    Возвращает:
    { 
        '<deployment_group': {
            '<deployment_task>': [
                {
                    'dg': <deployment_group>,
                    'tags': ['tag1', 'tag2'] ,
                    'args': ['arg1', 'arg2'],
                    'playbook': '/home/vasya/ansible/.../playbook.yml',
                    ....
                },
            ],
        },
    }

    '''
    logger.info('Парсинг yaml файлов...')
    deployment_tasks = config['deployment_tasks']
    deployment_groups = {}
    _i = 0
    for yaml_file in yaml_files:
        yaml_data = read_yaml(yaml_file, pass_errors=True)
        _i += 1
        if (_i % 400 == 0) or (_i == len(yaml_files)):
            logger.info('\t {} файлов прочитано'.format(_i))
        if not yaml_data:
            continue
        for hosts_item in yaml_data:
            if not isinstance(hosts_item, dict) or 'vars' not in hosts_item:
                continue
            if not hosts_item['vars']:
                continue
            elif 'hosts' not in hosts_item:
                continue
            if '_vars_files' in hosts_item['vars']:
                for vars_file in hosts_item['vars_files']:
                    if not vars_file.startswith('/'):
                        vars_file = vars_file.split('/')
                        #Берет директорию где лежит плейбук и подставляет в корень vars файла
                        vars_file = yaml_file.split('/')[:-1] + vars_file
                        vars_file = '/'.join(vars_file)
                    hosts_item['vars'].update(read_yaml(vars_file))
            if not hosts_item.get('vars',None):
                continue
            hosts = hosts_item['hosts']
            tasks_data = _parse_hosts_vars(config,
                                          hosts_item['vars'],
                                          deployment_tasks,
                                          playbook=yaml_file,
                                          hosts=hosts)
            if tasks_data:
                _update_deployment_groups(tasks_data, deployment_groups)
    return deployment_groups 
    
def _parse_hosts_vars(config, hosts_vars, deployment_tasks, playbook, **kwargs):
    """
    config : dict 
    hosts_vars : dict
        Словарь с переменными (vars) из плейбука конкретного hosts item
    deployment_tasks: set | list
        Список разрешенных DT, которые будут искаться в переменных
    playbook : string 
        Путь к обрабатываемому плейбуку
    kwargs : kwargs
        Все объекты отсюда будут подставлены в -e флаги ansible-playbook

    Ищет в vars таски и возвращает следующую структуру
    {
        '<task>': [ 
                    {
                        'dg': 'domain.ru_year',
                        'tags': ...,
                        ...
                     },
                ],
        '<task>': [{}],
        ...
    }
    """
    if isinstance(hosts_vars, list):
        return
    result = {}
    tasks_data = []
    #Поиск DT
    for dt in deployment_tasks:
        if dt in hosts_vars:
            tasks_data.append((dt, hosts_vars[dt]))
    global_dg = hosts_vars.get('_deployment_groups', None)
    #Парсинг DT
    for dt, data in tasks_data:
        if dt not in result:
            result[dt] = []
        if 'deployment_groups' not in data and not global_dg:
            message = 'Неправильный формат метаданных. Нет deployment_groups в таске {}, файл {}'
            logger.warning(message.format(dt, playbook))
            continue
        elif global_dg:
            message = 'Внимание, используются глобальные DG. Файл {}'
            logger.debug(message.format(playbook))
            data['deployment_groups'] = global_dg
        global_args = data.get('args', None)
        global_tags = data.get('tags', None)
        #Добавление глобальных переменных
        if global_tags:
            logger.debug('Внимание, используются глобальные ansible tags. Файл {}'.format(playbook))
        if global_args:
            logger.debug('Внимание, используются глобальные ansible args. Файл {}'.format(playbook))
        for dg_original in data['deployment_groups']:
            dg = copy.deepcopy(dg_original)
            if not 'args' in dg:
                dg['args'] = []
            #Добавление переменых из аргументов командной строки
            if len(config['environment']) > 0:
                dg['args'] += config['environment']
            #Добавление из kwargs
            for key,value in kwargs.items():
                dg[key] = value
            dg['playbook'] = playbook
            if data.get('nolimit', False):
                dg['hosts'] = None
            if config['limit']:
                dg['hosts'] = config['limit']
            if global_args:
                dg['args'] += global_args
            if global_tags:
                if not 'tags' in dg:
                    dg['tags'] = []
                dg['tags'] += global_tags
            if not 'dg' in dg:
                message = 'Неправильный формат метаданных. Нет "dg" в deployment task: {}, файл {}'
                logger.warning(message.format(dt, playbook))
                logger.debug(dg)
                continue
            if not 'tags' in dg and not dg.get('tags', None) and not data.get('notags', False):
                message = 'Неправильный формат метаданных. Нет ansible tags в DT: {}, файл {}'
                logger.warning(message.format(dt, playbook))
                logger.debug(dg)
                continue
            #Магия. Добавление всех items DG как environment для запуска плейбука
            for key, value in dg.items():
                if key in ("args","tags","hosts","playbook"):
                    continue
                if len(str(value).split(' ')) > 0:
                    dg['args'].append('-e \'{}="{}"\''.format(key,value))
                else:
                    dg['args'].append('-e "{}={}"'.format(key,value))

            result[dt].append(dg)
    return result

def _update_deployment_groups(tasks_data, deployment_groups):
    #Модифицирует словарь deployment_grous, добавляя в него данные из _parse_hosts_vars
    #Описание deployment_groups в функции _find_deployment_groups
    for task, _deployment_groups in tasks_data.items():
        for deployment_group in _deployment_groups:
            dg_name = deployment_group['dg']
            if dg_name not in deployment_groups:
                deployment_groups[dg_name] = {}
            if task not in deployment_groups[dg_name]:
                deployment_groups[dg_name][task] = []
            deployment_groups[dg_name][task].append(deployment_group)
    return deployment_groups


def _find_yaml_files(config):
    #Ищет все файлы в config['root_dir'] с указанными в config['extensions'] расширениями
    root_dir = config.get('root_dir')
    if not root_dir:
        root_dir = "~/ansible-ng"
    extensions = config.get('extensions')
    if not extensions:
        extensions = ['.yml', '.yaml']
    if root_dir.startswith('~/'):
        root_dir = Path(root_dir).home().joinpath(root_dir[2:])
    logger.info('Поиск {} файлов в {}'.format(extensions, root_dir))
    finded_files = []
    for extension in extensions:
        extension = "**/*{}".format(extension)
        finded_files += [str(x) for x in Path(root_dir).glob(extension)]
    finded_files = [x for x in finded_files if os.path.isfile(x)]
    return finded_files

def read_yaml(path, pass_errors=False):
    logger.debug('Open {}'.format(path))
    with open(str(path), 'r') as stream:
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as e:
            if not pass_errors:
                raise e
            else:
                logging.info('Ошибка при открытии файла {}'.format(path))
