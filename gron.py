#!/usr/bin/env python3
import argparse
import logger as logger_module
import logging
import discovery
import pprint
import sys
import os


def parse_arguments():
    description = """
    ./main.py -s - вывести текущее состояние, которое будет использоваться при раскатках.
    ./main.py -g domain.ru_year -t _deploy_cert -CD - запустить все раскатки из группы domain.ru_year с флагами ansible playbook -CD
    ./main.py -g domain.ru_year -t _deploy_cert --dry-run - выведет все запускаемые команды, но не запустит их.
    """
    parser = argparse.ArgumentParser(description=description, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-c','--config', help="Путь к конфигурационному файлу")
    parser.add_argument('-nc','--no-config', action='store_true', help="Не использовать конфигурационный файл")
    parser.add_argument('-s','--show', action='store_true', help="Показать все собранные данные")
    parser.add_argument('-sg','--show-dg', action='store_true', help="Показать только список deployment groups с доступными тегами")
    parser.add_argument('-g', '--deployment-group', help="Deployment group для которой будут запущены задачи")
    parser.add_argument('-t','--deployment-task', help="Запустить с указанным deployment task")
    parser.add_argument('-C','--ansible-dry-run', action='store_true', help="Запустить с флагом -C в каждом плейбуке")
    parser.add_argument('-D', '--ansible-debug', action='store_true', help="Запуск с -D в каждом плейбуке")
    parser.add_argument('-e', '--environment', action='append', help="Дополнительные переменные, напр. -e \"a='b' c='d'\"")
    parser.add_argument('-l', '--limit', help="Ограничить выполнение на определенных хостах")
    parser.add_argument('--root-dir', help="Директория с плейбуками, по-умолчанию ~/ansible-ng")
    parser.add_argument('--extensions', help="Расширения искомых файлов, по-умолчанию .yml и .yaml")
    parser.add_argument('--ansible-bin', help="Путь к ansible-playbook, по-умолчанию /usr/bin/ansible-playbook")
    parser.add_argument('--dry-run', action='store_true', help="Холостой запуск (показывает вызываемые команды без их запуска)")
    parser.add_argument('--debug', action='store_true', help="Дебаг")
    parser.add_argument('--silent', action='store_true', help="Вывод только critical сообщений")
    parser.add_argument('--skip-dg-notfound', action='store_true', help="Не выводить ошибку если DG не найдена")
    return parser.parse_args()


def setup_logger(args):
    options = {'debug': False, 'silent': False}
    if args.debug:
        options['debug'] = True
    if args.silent:
        options['silent'] = True
    logger_module.setup(options=options)
    global logger
    logger = logging.getLogger('main')

def load_config(args):
    config_path = None
    if not args.config and not args.no_config:
        current_executed_file = os.path.realpath(__file__)
        current_dir = '/'.join(current_executed_file.split('/')[:-1])
        config_path = os.path.join(current_dir, 'config.yml')
        if not os.path.exists(config_path):
            config_path = None
    else:
        config_path = args.config
    if config_path:
        logger.info('Конфиг: {}'.format(config_path))
        config = discovery.read_yaml(config_path)
    else:
        config = {}
    for arg_key, arg_value in args.__dict__.items():
        config[arg_key] = arg_value
    config['deployment_tasks'] = ['_deploy_cert', '_certbot_acme','_certbot_upload','_gcore_upload']
    return config


if __name__ == '__main__':
    args = parse_arguments()
    if args.environment:
        tmp_env = []
        for arg in args.environment:
            if '=' not in arg:
                continue
            arg = '-e {}'.format(arg)
            tmp_env.append(arg)
        args.environment = tmp_env
    else:
        args.environment = []
    setup_logger(args)
    config = load_config(args)
    required_arguments = [
        args.show,
        args.show_dg,
        args.deployment_group and args.deployment_task
    ]
    if not any(required_arguments):
        print('Нет требуемых агрументов')
        sys.exit(1)
    deployment_groups = discovery.get_deployment_groups(config)
    if args.show:
        print('\n')
        print(deployment_groups)
        sys.exit(0)
    if args.show_dg:
        print('\n')
        print(deployment_groups.show_dg())
        sys.exit(0)
    if not args.deployment_group and not args.deployment_task:
        print('Требуется указать deployment group (-g) и deployment task (-t)')
        sys.exit(1)
    deployment_groups.run(args.deployment_group, args.deployment_task)


